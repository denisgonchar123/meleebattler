// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MeleeBattler : ModuleRules
{
	public MeleeBattler(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "GameplayAbilities" });

        PrivateDependencyModuleNames.AddRange(new string[] { });

        PrivateIncludePaths.AddRange(new string[] { Name });

        
    }
}
