// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MBItem.h"
#include "MBMeleeItem.generated.h"


class USkeletalMeshComponent;
class UMBComboAttackData;

UCLASS()
class MELEEBATTLER_API AMBMeleeItem : public AMBItem
{
	GENERATED_BODY()

public:
	
	AMBMeleeItem();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneComponent* TransformComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USkeletalMeshComponent* MeshComponent;

public:

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Equip")
	FName AttachSocketName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Combo attack")
	UMBComboAttackData* ComboAttackData;


};
