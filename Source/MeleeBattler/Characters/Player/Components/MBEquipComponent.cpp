
#include "Characters/Player/Components/MBEquipComponent.h"
#include "Items/MBMeleeItem.h"
#include "../MBBaseCharacter.h"

UMBEquipComponent::UMBEquipComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void UMBEquipComponent::BeginPlay()
{	
	Super::BeginPlay();

	CachedBaseCharacter = StaticCast<AMBBaseCharacter*>(GetOwner());
}

void UMBEquipComponent::EquipItemClassToHand(TSubclassOf<AMBMeleeItem> NewItem)
{
	if (NewItem != nullptr)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		AMBMeleeItem* ItemInstance = GetWorld()->SpawnActor<AMBMeleeItem>(SpawnParameters);

		EquipItemToHand(ItemInstance);
	}
}

void UMBEquipComponent::EquipItemToHand(AMBMeleeItem* NewItem)
{
	if (NewItem != nullptr)
	{
		if (ActiveItem != nullptr)
		{
			ActiveItem->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
			ActiveItem->Destroy();
		}

		ActiveItem = NewItem;
		
		const USkeletalMeshComponent* MeshComponent = GetOwner<ACharacter>()->GetMesh();
		//ActiveItem->AttachToComponent(MeshComponent, FAttachmentTransformRules::SnapToTargetIncludingScale, NandItemAttachSocketName);
		ActiveItem->AttachToComponent(CachedBaseCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, NandItemAttachSocketName);

	}


}

AMBMeleeItem* UMBEquipComponent::GetActiveItem() const
{
	return ActiveItem;
}
