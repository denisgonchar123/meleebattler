// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MBEquipComponent.generated.h"

class AMBMeleeItem;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MELEEBATTLER_API UMBEquipComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UMBEquipComponent();

	virtual void BeginPlay() override;

public:	
	UFUNCTION(BlueprintCallable)
	void EquipItemClassToHand(TSubclassOf<AMBMeleeItem> NewItem);

	UFUNCTION(BlueprintCallable)
	void EquipItemToHand(AMBMeleeItem* NewItem);
	
	UFUNCTION(BlueprintCallable)
	AMBMeleeItem* GetActiveItem() const;

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName NandItemAttachSocketName;


private:
	AMBMeleeItem* ActiveItem;

	TWeakObjectPtr<class AMBBaseCharacter> CachedBaseCharacter;
};
