// Fill out your copyright notice in the Description page of Project Settings.


#include "MBBaseCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "CharacterMovementComponent/MBBaseCharacterMovementComponent.h"
#include "Components/MBEquipComponent.h"
#include "Components/MBComboAttackComponent.h"


AMBBaseCharacter::AMBBaseCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UMBBaseCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	BaseCharecterMovementComponent = StaticCast<UMBBaseCharacterMovementComponent*>(GetCharacterMovement());

	//PrimaryActorTick.bCanEverTick = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring arm"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bUsePawnControlRotation = true;

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	FollowCamera->SetupAttachment(CameraBoom);
	FollowCamera->bUsePawnControlRotation = false;

	GetCharacterMovement()->bOrientRotationToMovement = 1;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 700;

	CharacterEquipComponent = CreateDefaultSubobject<UMBEquipComponent>(TEXT("EquipComponent"));

	CharacterComboAttackComponent = CreateDefaultSubobject<UMBComboAttackComponent>(TEXT("ComboAttackComponent"));


}

void AMBBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void AMBBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


UMBEquipComponent* AMBBaseCharacter::GetEquipComponent() const
{
	return CharacterEquipComponent;
}

UMBComboAttackComponent* AMBBaseCharacter::GetComboAttackComponent() const
{
	return CharacterComboAttackComponent;
}

bool AMBBaseCharacter::CanJumpInternal_Implementation() const
{
	return Super::CanJumpInternal_Implementation();
}

void AMBBaseCharacter::OnJumped_Implementation()
{
	//DOTO 
}


