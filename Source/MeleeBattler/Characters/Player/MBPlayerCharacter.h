#pragma once

#include "CoreMinimal.h"
#include "Characters/Player/MBBaseCharacter.h"
#include "MBPlayerCharacter.generated.h"


UCLASS()
class MELEEBATTLER_API AMBPlayerCharacter : public AMBBaseCharacter
{
	GENERATED_BODY()
	
public:
	
	AMBPlayerCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

protected:

	virtual void MoveForward(float Value) override;
	virtual void MoveRight(float Value) override;
	virtual void Turn(float Value) override;
	virtual void LookUp(float Value) override;

};
