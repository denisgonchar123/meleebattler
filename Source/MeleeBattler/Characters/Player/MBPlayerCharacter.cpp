// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/Player/MBPlayerCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

AMBPlayerCharacter::AMBPlayerCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	
	


}

void AMBPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();


}

void AMBPlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

void AMBPlayerCharacter::MoveForward(float Value)
{
	if ((GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling()) && !FMath::IsNearlyZero(Value, 1e-6f))
	{
		FRotator YawRotator(0.0f, GetControlRotation().Yaw, 0.0f);
		FVector ForwardVector = YawRotator.RotateVector(FVector::ForwardVector);
		AddMovementInput(ForwardVector, Value);

	}
}

void AMBPlayerCharacter::MoveRight(float Value)
{
	if ((GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling()) && !FMath::IsNearlyZero(Value, 1e-6f))
	{
		FRotator YawRotator(0.0f, GetControlRotation().Yaw, 0.0f);
		FVector RightVector = YawRotator.RotateVector(FVector::RightVector);
		AddMovementInput(RightVector, Value);

	}
}

void AMBPlayerCharacter::Turn(float Value)
{
	AddControllerYawInput(Value);
}

void AMBPlayerCharacter::LookUp(float Value)
{
	AddControllerPitchInput(Value);
}
