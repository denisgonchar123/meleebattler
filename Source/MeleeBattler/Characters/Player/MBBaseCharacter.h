// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MBBaseCharacter.generated.h"

class UMBBaseCharacterMovementComponent;
class UMBEquipComponent;
class UMBComboAttackComponent;


UCLASS()
class MELEEBATTLER_API AMBBaseCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:
	AMBBaseCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	virtual void MoveForward(float Value) {};
	virtual void MoveRight(float Value) {};
	virtual void Turn(float Value) {};
	virtual void LookUp(float Value) {};

	virtual bool CanJumpInternal_Implementation() const override;
	virtual void OnJumped_Implementation() override;

public:
	
	UMBEquipComponent* GetEquipComponent() const;
	UMBComboAttackComponent* GetComboAttackComponent() const;

	FORCEINLINE UMBBaseCharacterMovementComponent* GetBaseCharacterMovementComponent() const { return BaseCharecterMovementComponent;}

protected:
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UMBEquipComponent* CharacterEquipComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UMBComboAttackComponent* CharacterComboAttackComponent;

	UMBBaseCharacterMovementComponent* BaseCharecterMovementComponent;
};
