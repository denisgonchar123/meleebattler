// Fill out your copyright notice in the Description page of Project Settings.


#include "MBBaseCharacterMovementComponent.h"
#include "../MBBaseCharacter.h"

void UMBBaseCharacterMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	BaseCharacterOwner = Cast<AMBBaseCharacter>(GetOwner());
}

AMBBaseCharacter* UMBBaseCharacterMovementComponent::GetBaseCharacterOwner() const
{
	return StaticCast<AMBBaseCharacter*>(BaseCharacterOwner);
}
