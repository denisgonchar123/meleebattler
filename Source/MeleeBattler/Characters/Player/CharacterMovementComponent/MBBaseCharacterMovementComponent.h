// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MBBaseCharacterMovementComponent.generated.h"

class AMBBaseCharacter;

UCLASS()
class MELEEBATTLER_API UMBBaseCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;


protected:
	
	AMBBaseCharacter* BaseCharacterOwner;

	AMBBaseCharacter* GetBaseCharacterOwner() const;
};
