// Fill out your copyright notice in the Description page of Project Settings.


#include "BMBaseCharacterController.h"
#include "../MBBaseCharacter.h"


void ABMBaseCharacterController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	CachedBaseCharacter = Cast<AMBBaseCharacter>(InPawn);

}

void ABMBaseCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveForward", this, &ABMBaseCharacterController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ABMBaseCharacterController::MoveRight);
	InputComponent->BindAxis("Turn", this, &ABMBaseCharacterController::Turn);
	InputComponent->BindAxis("LookUp", this, &ABMBaseCharacterController::LookUp);

	InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &ABMBaseCharacterController::Jump);

}

void ABMBaseCharacterController::MoveForward(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->MoveForward(Value);

	}
}

void ABMBaseCharacterController::MoveRight(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->MoveRight(Value);

	}
}

void ABMBaseCharacterController::Turn(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->Turn(Value);

	}
}

void ABMBaseCharacterController::LookUp(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->LookUp(Value);

	}
}

void ABMBaseCharacterController::Jump()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->Jump();
	}
}
