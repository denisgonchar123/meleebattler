// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "MBComboAttackData.generated.h"

/**
 * 
 */
UCLASS()
class MELEEBATTLER_API UMBComboAttackData : public UDataAsset
{
	GENERATED_BODY()
	
};
